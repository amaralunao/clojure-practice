(ns clojure-noob.sequences-example)

(def fibs
  (map first
       (iterate (fn [[a b]] 
             [b (+ a b)])
           [0 1])))

;; (set! *print-length* 10) in REPL
;; (take 5 fibs) # (0 1 1 2 3)
;; (drop 5 fibs) # (5 8 13 21 34 55 89 144 233 377 ...)
;; (map inc (take 5 fibs)) # (1 2 2 3 4)
