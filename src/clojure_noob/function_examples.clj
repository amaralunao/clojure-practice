(ns clojure-noob.function-examples)

(let [x 10
      y 21]
  (+ x y))

(defn messenger
  ([] (messenger "Hello world!"))
  ([msg] (print msg)))

(defn messenger1 [greeting & who]
  (print greeting who))
;; who is a sequence

(defn messenger2 [greeting & who]
  (apply print who))

;; apply will "unpack" the who list and pass it to print

(let [numbers '(1 2 3)]
  (apply + numbers))
;; output is 6 (apply + to numbers)


(defn messenger-builder [greeting]
  (fn [who] (print greeting who)))  ; closes over greeting

;; greeting provided here, then goes out of scope
(def hello-er (messenger-builder "Hello"))

;; greeting still available because hello-er is closure
;; (hello-er "world!") will evaluate to Hello world!

