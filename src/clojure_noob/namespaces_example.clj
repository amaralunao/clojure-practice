(ns clojure-noob.namespaces-examples
  (:require [clojure.set :as set])
  (:use [clojure.java.io :only (delete-file)]))

(defn do-unit [& sets]
  (apply set/union sets))

(defn delete-old-files [& files]
  (doseq [f files]
    (delete-file f)))
