(ns clojure-noob.flow-control)

(defn show-evens [coll]
  (if-let [evens (seq (filter even? coll))]
    (println (str "The evens are: " evens))
    (println "There were no evens.")))

(defn str-binary [n]
  (case n
    0 "zero"
    1 "one"
    "unknown"))

;; case is the least general => requires compile time values
;; on the left hand side 
;; and always uses equality for it's predicate
;;
;;(defn str-binary [n]
;;  (condp = n
;;    0 "zero"
;;    1 "one"
;;    "unknown"))
;;
;; condp allows you to share predicates
;;
;;(defn str-binary [n]
;;  (cond
;;    (= n 0) "zero"
;;    (= n 1) "one"
;;    :else "unknown"))
;;
;; cond is the most general

(defn factorial
  ([n] (factorial 1 n))
  ([accum n]
   (if (zero? n)
     accum
     (recur (*' accum n)(dec n)))))

;; recur mechanism takes over and unrolls our functions into a loop


;;(defn factorial
;;  ([n] (factorial 1 n))
;;  ([accum n]
;;   (if (zero? n)
;;     accum
;;     (factorial (*' accum n)(dec n)))))
;; tail recursive function == call to itself is the last expression in the if branch
